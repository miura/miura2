"""
Plot single track for evaluations.
(1) loads track data file and extracts assigned id single track
(2) from image source, extracts a small 4D sequence at the corresponding position of the selected track
(3) volume rendering in 3D viewer with time lapse, and overlay the track. 
Kota Miura (miura@embl.de)
20120224
"""
from emblcmci.view3d import Plot4d
#from emblcmci.view3d import DialogVisualizeTracks
from javax.vecmath import Point3f
from javax.media.j3d import Transform3D
from javax.vecmath import Vector3f
from java.util import ArrayList
import os

from javax.vecmath import Color3f

from ij3d import Content
from ij3d import ContentCreator
from ij3d import Image3DUniverse
from ij3d.behaviors import ViewPlatformTransformer
from customnode import CustomLineMesh
from customnode import CustomMultiMesh
from ij import IJ, ImagePlus
from emblcmci import Extractfrom4D
from ij.io import FileOpener
from ij.plugin import FileInfoVirtualStack
from jarray import array
from ij.plugin import Concatenator


def oneTimePointFrom4D(imp):
	e4d = Extractfrom4D()
	e4d.setGstarttimepoint(1)
	IJ.log("current time point" + str(1))
	aframe = e4d.coreheadless(imp, 3)
	return aframe
	
def getSubstack(imgpath, zmin, zmax, timepoint):

	td = TiffDecoder(os.path.dirname(imgpath) , os.path.basename(imgpath))
	fileinfoA = td.getTiffInfo()
	fo1 = fileinfoA[0].clone()
	fo1.nImages = 1
	fo = FileOpener(fo1);
	props = fo.decodeDescriptionString(fileinfoA[0])
	#print props.toString()
	frames = int(props['frames'])
	slices = int(props['slices'])
	print 'frames', frames, 'slices', slices
	imp = fo.open(False);
	calib = imp.getCalibration()
	xys = calib.pixelWidth
	zs = calib.pixelDepth
	print 'xy scales', xys, 'zscale', zs
	topslice = Math.floor(zmin / zs)
	bottomslice = Math.ceil(zmax / zs)
	print "slice range:", topslice, bottomslice
	if topslice <0:
		topslice = 0
		print "topslice reset to 0"
	if bottomslice > slices-1:
		bottomslice = slices-1
		print "bottom slice set to ", imp.getNSlices()-1
	vstack = FileInfoVirtualStack(fileinfoA[0], False)
	print "vstack size", vstack.getSize()
	frameoffset = slices * timepoint
	substack = ImageStack(imp.getWidth(), imp.getHeight())
	for i in range(topslice, bottomslice+1):
 		ip = vstack.getProcessor(frameoffset + i + 1)
 		substack.addSlice('z'+str(i)+ 't'+str(timepoint), ip)
 	outimp = ImagePlus("substack", substack)
 	outimp.setCalibration(calib)
 	return outimp
 			
univ = Image3DUniverse()


srcpath = '/Users/miura/Dropbox/Mette/20_23h/20_23hrfull_corrected_1_6_6.csv'
#srcpath = 'C:/dropbox/My Dropbox/Mette/20_23h/20_23hrfull_corrected_1_6_6.csv'
destpath = os.path.dirname(srcpath) + os.sep

p4d = Plot4d(srcpath, Plot4d.DATATYPE_VOLOCITY)
tList = p4d.getTrajlist()	
trackid = 13	# used for the presentation: 8
atrack = tList.get(trackid).getDotList()
atimepoints = tList.get(trackid).getTimepoints()
bnd = Plot4d.getBoudingBox(atrack) # arraylist of Float

imgpath = '/User/miura/Desktop/20h_shifted_f01_03.tif'
imgpath = 'Z:/mette/20_23h_firstSample/20h_shifted.tif'
imgpath = '/Volumes/cmci/mette/20_23h_firstSample/20h_shifted.tif'
timeseries = []
for tt in atimepoints:
	subsubimp = getSubstack(imgpath, bnd.get(2), bnd.get(5), tt)
	timeseries.append(subsubimp)
calib = timeseries[0].getCalibration()
jaimp = array(timeseries, ImagePlus)
ccc = Concatenator()
subimp = ccc.concatenate(jaimp, True)
subimp.setCalibration(calib)
frames = atimepoints.size()
slices = subimp.getStackSize()/frames
subimp.setDimensions(1, slices, frames)

#tstack = ImageStack(timeseries.get(0).getWidth(), timeseries.get(0).getHeight())
#for astack in timeseries:
#	tstack.add()

#imp = ImagePlus(imgpath)
#imp1 = oneTimePointFrom4D(imp)
calib = subimp.getCalibration()
xys = calib.pixelWidth
zs = calib.pixelDepth
offset = 0 #micrometers

left = int((bnd.get(0)- offset)/xys) 
top  = int((bnd.get(1)- offset)/xys) - offset
ww = int((bnd.get(3) - bnd.get(0) + 2 * offset) / xys)
hh = int((bnd.get(4) - bnd.get(1) + 2 * offset) / xys)
xycropstk = StackProcessor(subimp.getStack(), None).crop(left, top, ww, hh)
xycropimp = ImagePlus('vol', xycropstk)
xycropimp.setCalibration(calib)
xycropimp.setDimensions(1, slices, frames)
xycropimp.setOpenAsHyperStack(True)
#xycropimp.show()
print 'cropped from x-y-ww-hh',left, top, ww, hh 
univ.show()
obj = univ.addVoltex(xycropimp)
#obj = univ.addSurfacePlot(xycropimp, Color3f(255.0, 0.0, 0.0), "surf", 150, jba , 2)
#obj = univ.addSurfacePlot(xycropimp)
obj.setThreshold(0)
obj.setTransparency(0.1)
obj.setLocked(True) 
#setTransparency(float transparency) 

dx = bnd.get(0)- offset
dy = bnd.get(1)- offset
dz = Math.floor(bnd.get(2) / zs)*zs
for i in range(frames):
	trackmesh = p4d.createSingleTrackMesh(tList, trackid) #Content
	#trackmesh = p4d.createSingleTrackMeshShifted(tList, trackid, dx, dy, dz) #Content
	cont =ContentCreator.createContent(trackmesh, "Track"+str(i), i)
	t3d = Transform3D() 
	trans = Point3f(-1 * dx,-1 * dy, -1 * dz)
	t3d.setTranslation(Vector3f(trans))
	cont.setTransform(t3d)
	univ.addContent(cont);
#univ.addContent(ccs)

univ.centerSelected(obj)
vtf = ViewPlatformTransformer(univ, univ)
vtf.zoomTo(10)





